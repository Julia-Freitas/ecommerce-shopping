import React from 'react';
import './SecaoProdutosVendidos.css';
import ProdutoGeral from '../produtoGeral/ProdutoGeral';

function SecaoProdutosVendidos() {
  return (
    <div className="produtos-vendidos">
      <h2>Produtos Mais Vendidos</h2>
      <div className="lista-produtos">
        <ProdutoGeral />
        <ProdutoGeral />
        <ProdutoGeral />
      </div>    
    </div>
  )
}

export default SecaoProdutosVendidos;
