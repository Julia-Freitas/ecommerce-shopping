import React from 'react';
import './ProdutoGeral.css';
import produto1 from '../../assets/img/produto1.jpg';

function ProdutoGeral() {
  return (
    <div className="produto-geral">
      <img src={produto1} alt="imagem do produto" />
      <div className="descricao-produto-geral">
        <p>Computador</p>
        <p>9999 vendido</p>
        <p>200 likes</p>
        <p>Eletrônico</p>
        <p>Sobre o Computador</p>
        <p>R$ 1000,00</p>
        <button className="bto-produtos">Ver Mais</button>
      </div>
    </div>
  )
}

export default ProdutoGeral;
