import React from 'react';
import './NaoEncontrada.css';

function NaoEncontrada() {
  return (
    <div className="nao-encontrada">
      <h1>Página Não Encontrada</h1>
      <p>Tente denovo, tenho fé que você vai conseguir acessar nosso site :)</p>
    </div>
  )
}

export default NaoEncontrada;
